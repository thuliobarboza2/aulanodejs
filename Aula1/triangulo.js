function triangulo1(a, b, c){
    if(a != 0 && b != 0 && c != 0){
        if(Math.abs(a-b) < c && a + b > c){
            if(Math.abs(c-b) < a && c + b > a){
                if(Math.abs(a-c) < b && c + b > b){
                    console.log("É triângulo.");
                }
                else{
                    console.log("Não é triângulo.");
                }
            }
            else{
                console.log("Não é triângulo.");
            }
        }
        else{
            console.log("Não é triângulo.");
        }
    }
    else{
        console.log("Não pode haver valor nulo.");
    }
}

function teste(a, b, c){
    if(Math.abs(a-b) < c && a + b > c) return true
    else return false
}

function triangulo2(a, b, c){
    if(a != 0 && b != 0 && c != 0){
        if(teste(a, b, c) && teste(a, c, b) && teste(b, c, a)){
            console.log("É triângulo.");
        }
        else{
            console.log("Não é triângulo.");
        }
    }
    else{
        console.log("Não pode haver valor nulo");
    }
}

triangulo1(1, 2, 3);
triangulo1(1, 2, 2);
triangulo1(1, 0, 3);


triangulo2(1, 2, 3);
triangulo2(1, 2, 2);
triangulo2(1, 0, 3);
