const express = require('express');
const app = express();
const porta = 3000;

app.set('view engine', 'ejs'); // Define EJS como o mecanismo de modelo

// Middleware para servir arquivos estáticos na pasta 'public'
app.use(express.static('public'));

// Rota para a página principal
app.get('/', (req, res) => {
  res.render('index');
});

// Rota para a página de contato
app.get('/contato', (req, res) => {
  res.render('contato', { mensagem: 'Bem-vindo à página de contato!' });
});

app.get('/sobre', (req, res) => {
    res.render('sobre', { mensagem: 'Bem-vindo à página sobre!' });
  });

app.listen(porta, () => {
  console.log(`Servidor rodando em http://localhost:${porta}`);
});
